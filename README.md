# Mandelrust
A mandelbrot set viewer in RUST, this is just meant to be a project made for fun and hopefully a way to create those amazing looking zoom animations. [Example](https://www.youtube.com/watch?v=pCpLWbHVNhk)  
## Dependencies
* Nom  
* SDL (1.2)
# Example output
![Example output](https://i.imgur.com/dNeBJxh.png)