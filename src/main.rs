#[macro_use]
extern crate nom;
extern crate sdl;

use sdl::event::{Event, Key};
use sdl::video::{SurfaceFlag, VideoFlag};

fn map(iter: i32, maxPx: i32, minRange: f64, maxRange: f64) -> f64 {
    // Example Input 455, 800, -2.0, 2.0
    // Calculates what points to calculate
    let distance: f64 = maxRange.abs() + (minRange.abs());
    let increment: f64 = distance / maxPx as f64;
    let result: f64 = (iter as f64 * increment) + minRange;
    return result;
}

fn colorize(_x: i32) -> (u8, u8, u8) {
    // Squares the input and converts it to hex, then it converts it to RGB
    // This is quite slow and should be replaced with something else
    let total_color: i32 = _x * _x * _x;
    let mut hex: String = format!("{:x}", total_color);
    let mut result = (0u8, 0u8, 0u8);
    if hex.len() < 6 {
        for _y in 0..6 - hex.len() {
            hex = "0".to_string() + &hex;
        }
    }
    let r = hex.get(0..2).unwrap();
    let g = hex.get(2..4).unwrap();
    let b = hex.get(4..6).unwrap();
    result.0 = u8::from_str_radix(&r, 16).unwrap();
    result.1 = u8::from_str_radix(&g, 16).unwrap();
    result.2 = u8::from_str_radix(&b, 16).unwrap();
    return result;
}

fn mandelbrot(posX: f64, posY: f64) -> Vec<u8> {
    // Placeholder value, to return color as [u8, u8, u8]
    // Mandelbrot function f(x) = x^2 + c
    // x being the posx and c being posy of the values returned from the map function

    // This code is absolutely disgusting
    let mut a: f64 = posX;
    let mut b: f64 = posY;
    let mut aa: f64 = 0.0;
    let mut bb: f64 = 0.0;
    let mut color = (0u8, 0u8, 0u8);
    let mut result: f64 = 0.0;
    // Iterates each pixel 255 times
    for _i in 0..255 {
        aa = a * a - b * b;
        bb = 2.0 * a * b;
        a = aa + posX;
        b = bb + posY;
        result = a.abs() + b.abs();
        if (result > 20.0) {
            // Colorizes the pixels that go to infinity
            color = colorize(_i);
            break;
        }
    }
    if (result < 20.0) {
        return vec![0 as u8, 0 as u8, 0 as u8];
    } else {
        return vec![color.0, color.1, color.2];
    }
}

fn main() {
    // Initialize and set window caption
    sdl::init(&[sdl::InitFlag::Video]);
    sdl::wm::set_caption("Mandelrust", "rust-sdl");

    let screenX: i32 = 1400;
    let screenY: i32 = 1400;
    let mut minX: f64 = -2.0;
    let mut maxX: f64 = 1.0;
    let mut minY: f64 = -1.5;
    let mut maxY: f64 = 1.5;

    // Set up screen
    let screen = match sdl::video::set_video_mode(
        screenX as isize,
        screenY as isize,
        32,
        &[SurfaceFlag::HWSurface],
        &[VideoFlag::DoubleBuf],
    ) {
        Ok(screen) => screen,
        Err(err) => panic!("failed to set video mode: {}", err),
    };
    'main: loop {
        // Clear screen with a white background
        screen.fill_rect(
            Some(sdl::Rect {
                x: (0 as i16),
                y: (0 as i16),
                w: screenX as u16,
                h: screenY as u16,
            }),
            sdl::video::Color::RGB(0, 0, 0),
        );

        // Calculating the entire screen
        for _y in 0..screenY {
            for _x in 0..screenX {
                let fineX: f64 = map(_x, screenX, minX, maxX);
                let fineY: f64 = map(_y, screenY, minY, maxY);
                let color = mandelbrot(fineX, fineY);
                // Drawing the pixels
                screen.fill_rect(
                    Some(sdl::Rect {
                        x: (_x as i16),
                        y: (_y as i16),
                        w: 1 as u16,
                        h: 1 as u16,
                    }),
                    sdl::video::Color::RGB(color[0 as usize], color[1 as usize], color[2 as usize]),
                );
            }
        }
        // Flip screen
        screen.flip();
        // Event loop for getting key input etc
        'event: loop {
            // Match (switch) case statement for exiting window and getting events for every frame
            match sdl::event::poll_event() {
                Event::Quit => break 'main,
                Event::None => break 'event,
                Event::Key(k, _, _, _) if k == Key::Escape => break 'main,
                _ => {}
            }
        }
    }

    sdl::quit();
}
